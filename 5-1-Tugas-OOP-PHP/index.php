<?php 

abstract class Hewan{
    public $nama;
    public static $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama){
        $this->nama = $nama;
    }

    public function atraksi(){
        return "Atraksi = " . $this->nama . " sedang " . $this->keahlian;
    }
    
}


trait Fight{
    public $attackPower;
    public $defencePower;

    public function serang(){
        return $this->nama . " serang " . $this->nama;
    }

    public function diserang($hewan){
        return $this->nama . " sedang diserang " . $hewan->nama;
        $this->darah = $this->darah - $this->attackPower / $this->defencePower;
    }
}

class Elang extends Hewan{

    protected $jumlahKaki = 2, $keahlian = "Terbang tinggi", $attackPower = 10, $defencePower = 5;
    
    public function getInfoHewan($nama, $jenis = "Elang"){
        $this->nama = $nama;
        $this->jenis = $jenis;

        echo "Nama hewan = $nama <br>";
        echo "Jenis hewan = $jenis <br>";
        echo "Darah = " . $this->darah - $this->attackPower/$this->defencePower . "<br>";
        echo "Jumlah kaki = $this->jumlahKaki <br>";
        echo "Keahlian = $this->keahlian <br>";
        echo "Attack Power = $this->attackPower <br>";
        echo "Defence Power = $this->defencePower <br>";
    }
}

class Harimau extends Hewan{
    protected $jumlahKaki = 4, $keahlian = "Berlari cepat", $attackPower = 7, $defencePower = 8;
    
    public function getInfoHewan($nama, $jenis = "Harimau"){
        $this->nama = $nama;
        $this->jenis = $jenis;

        echo "Nama hewan = $nama <br>";
        echo "Jenis hewan = $jenis <br>";
        echo "Darah = " . $this->darah - $this->attackPower/$this->defencePower . "<br>";
        echo "Jumlah kaki = $this->jumlahKaki <br>";
        echo "Keahlian = $this->keahlian <br>";
        echo "Attack Power = $this->attackPower <br>";
        echo "Defence Power = $this->defencePower <br>";
    }
}

$elang = new Elang();
$harimau = new Harimau();
echo $elang->getInfoHewan("Elang_1");
echo "<br>";
echo $harimau->getInfoHewan("Harimau_2");
echo $elang->diserang("aaa");

?>